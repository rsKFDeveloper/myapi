FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
EXPOSE 8080
EXPOSE 5000
WORKDIR /app
COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c release -o out

FROM mcr.microsoft.com/dotnet/aspnet:3.1
EXPOSE 8080
EXPOSE 5000
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet","MyWebAPI.dll", "--urls", "http://0.0.0.0:5000"]
